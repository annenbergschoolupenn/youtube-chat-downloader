# Diami Virgilio - YouTube Live Chat Downloader Request

**Etienne Jacquot - 02/15/2021**

## Getting Started

Our pipeline consists of three workflows:

1. [youtubeAPI_urls.ipynb](./youtubeAPI_urls.ipynb) which uses google developer key & Youtube APIv3 to get all URLs for the provided Youtube Playlist URL
- Make sure to create your google dev keys saved as **configs/config.ini** (this is not included in repo!)
- The playlist had 273 videos in 08/2020, now this has like 295 videos, though of course not all had live chats enabled

2. [chat-downloader.ipynb](./chat-downloader.ipynb) which takes URLs and gets messages & superchats returned as json outfiles
    - **TODO:** consider Docker workflow for this notebook... running for 180 videos is a *lot* of output printed and it crashed my notebook

3. [etl-simple-chats.ipynb](./etl-simple-chats.ipynb) which takes the raw json outfiles, run a YoutubeAPI call for each unique URL for basic info, and extracts the USD value counts from amount string... 
- We talked about internation currency (non-usd), I checked and not sure this is significant ($300 vs $67K)... something to consider for future projects, *how to use regexp to get _###.## to extract any val & current*...


### Data outfiles

The directory [data](./data) contains sample CSV with URLs, along with subdirectors [data/raw](./data/raw) and [data/etl](./data/etl)

### 
__________

### Google Developer --> YoutubeAPIv3 keys:

Navigate to https://console.developers.google.com/ and create a new Youtube API project, generate a new key and restrict for youtube as a dictionary pair in `./configs/config.ini`

### Chat-Downloader Repository

Using the git repo here: [https://github.com/xenova/chat-downloader](./https://github.com/xenova/chat-downloader) 

``` bash
pip install chat-downloader
```

### The archive directory has a lot of random stuff! : 


- [./archive/AthenaQuery.ipynb](./archive/AthenaQuery.ipynb) is notebook for reviewing Athena CSV export!

__________

## Etienne Jacquot - 08/07/2020


### IT Support Ticket `ID# 1216`:

Hi IT,

I'm wondering if there is anyone there who has experience with acquiring YouTube data? Specifically, I'm trying to download YouTube chat data somehow from past live streams. I see there are some tools to do so using python, but I confess to not knowing anything about python and I wonder if maybe someone has asked for something like this before? All of the videos I'm looking at come from the same channel, that is: https://www.youtube.com/user/YCarnell

In particular, I am looking to get access to chats on this account's livestreams, which are all collected in this playlist: https://www.youtube.com/playlist?list=PLwlpJmhLD5o3OzRXFvU4I_5boeuWO64W9


I'm not sure if there is a quick, clever way to do this, especially for a batch of videos, but if so it would be much appreciated. 
________


